import pandas as pd
from sklearn.impute import SimpleImputer
from sklearn.ensemble import GradientBoostingRegressor, AdaBoostRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

# Создайте DataFrame с вашими данными
data = pd.DataFrame({
    'Unique number': [1, 2, 3, 4, 5],
    'Возраст, полных лет': [30, 36, 46, 45, 48],
    'Гражданство': ['Казахстан', 'Казахстан', 'Казахстан', 'Казахстан', 'Казахстан'],
    'Стаж вождения': [10, 17, 28, 13, 18],
    'Пол': ['М', 'М', 'М', 'М', 'М'],
    'КБМ': [8, 8, 8, 7, 9],
    'Город': ['Алматы', 'Нур-Султан', 'Алматы', 'Нур-Султан', 'Затобольск'],
    'Марка': ['Alfa romeo', 'Alfa romeo', 'Aprilia', 'Audi', 'Audi'],
    'Модель': ['Легковые автомобили', 'Легковые автомобили', 'Мотоциклы и мотороллеры', 'Легковые автомобили', 'Легковые автомобили'],
    'Тип ТС': ['12/1/2000', '12/1/2001', '12/1/2005', '12/1/1992', '12/1/1995'],
    'Год выпуска': [2000, 2001, 2005, 1992, 1995],
    'Расположение руля': ['Слева', 'Слева', 'Слева', 'Слева', 'Слева'],
    'Объём двигателя': [3000, 2387, 998, 1984, 2800],
    'Цвет': ['красный', 'синий', 'красный', 'серебристый металлик', 'серый металлик'],
    'Период страхования': ['14.06.2013-13.06.2014', '19.07.2013-18.07.2014', '07.06.2013-06.06.2014', '30.05.2013-29.11.2013', '28.05.2013-27.05.2014'],
    'Страховая премия': [17625, 8484, 8031, 6709, 8257],
    'Льготы': [None, None, None, None, None],
    'Дата проишествия': [None, None, None, None, None],
    'Регион ДТП': [None, None, None, None, None],
    'Сумма убытка': [None, None, None, None, None]
})

# Разделите столбец 'Период страхования' на два столбца: начальный и конечный период страхования
data[['Начальный период', 'Конечный период']] = data['Период страхования'].str.split('-', expand=True)

# Преобразуйте новые столбцы в числовой формат
data['Начальный период'] = pd.to_datetime(data['Начальный период'], format='%d.%m.%Y')
data['Конечный период'] = pd.to_datetime(data['Конечный период'], format='%d.%m.%Y')

# Вычислите разницу в месяцах между начальным и конечным периодом страхования
data['Период страхования'] = (data['Конечный период'] - data['Начальный период']) / pd.Timedelta(days=30)

# Удалите временные столбцы 'Начальный период' и 'Конечный период'
data = data.drop(['Начальный период', 'Конечный период'], axis=1)

# Преобразуйте категориальные признаки в числовые с помощью one-hot encoding
data = pd.get_dummies(data, columns=['Гражданство', 'Пол', 'Город', 'Марка', 'Модель', 'Тип ТС', 'Расположение руля', 'Цвет', 'Льготы'])

# Разделите данные на признаки (X) и целевую переменную (y)
X = data.drop('Страховая премия', axis=1)
y = data['Страховая премия']

# Создайте и примените SimpleImputer для заполнения пропущенных значений в числовых столбцах
imputer = SimpleImputer(strategy='median')
X = imputer.fit_transform(X)

# Разделите данные на обучающий и тестовый наборы
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Создайте и обучите модель градиентного бустинга
gb_model = GradientBoostingRegressor(n_estimators=100, learning_rate=0.1, max_depth=3, random_state=42)
gb_model.fit(X_train, y_train)

# Получите прогнозы на тестовом наборе для градиентного бустинга
y_pred_gb = gb_model.predict(X_test)

# Оцените качество модели градиентного бустинга
mse_gb = mean_squared_error(y_test, y_pred_gb)
print("Mean Squared Error (Gradient Boosting):", mse_gb)

# Создайте и обучите модель AdaBoost
ab_model = AdaBoostRegressor(n_estimators=100, learning_rate=0.1, random_state=42)
ab_model.fit(X_train, y_train)

# Получите прогнозы на тестовом наборе для AdaBoost
y_pred_ab = ab_model.predict(X_test)

# Оцените качество модели AdaBoost
mse_ab = mean_squared_error(y_test, y_pred_ab)
print("Mean Squared Error (AdaBoost):", mse_ab)


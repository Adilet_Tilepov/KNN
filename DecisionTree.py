# Импортируем необходимые библиотеки
from sklearn.datasets import load_iris
from sklearn.tree import DecisionTreeClassifier

# Загружаем набор данных Iris
iris = load_iris()
X = iris.data
y = iris.target

# Создаем экземпляр классификатора дерева решений
clf = DecisionTreeClassifier()

# Обучаем классификатор на данных Iris
clf.fit(X, y)

# Делаем прогноз на новых данных
new_data = [[5.1, 3.5, 1.4, 0.2]]  # Пример новых данных для классификации
predicted_class = clf.predict(new_data)

# Выводим предсказанный класс
print(f"Предсказанный класс: {predicted_class[0]}")

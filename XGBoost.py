import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import pandas as pd

# Создаем ручной датасет о недвижимости
data = pd.DataFrame({
    'Size (sqft)': [1000, 1200, 1400, 1600, 1800, 2000, 2200, 2400, 2600, 2800],
    'Bedrooms': [2, 3, 3, 3, 4, 4, 4, 5, 5, 5],
    'Bathrooms': [1, 2, 2, 2, 2, 3, 3, 3, 4, 4],
    'Price ($)': [120000, 150000, 170000, 200000, 220000, 250000, 270000, 300000, 320000, 350000]
})

# Подготовка данных
X = data.drop('Price ($)', axis=1)
y = data['Price ($)']

# Разделяем данные на обучающий и тестовый наборы
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Определяем параметры модели XGBoost
params = {
    'max_depth': 3,
    'objective': 'reg:squarederror',  # Используем регрессионный объект
    'eta': 0.1
}

# Создаем объект DMatrix для оптимизации производительности
dtrain = xgb.DMatrix(X_train, label=y_train)
dtest = xgb.DMatrix(X_test, label=y_test)

# Обучаем модель
num_round = 100
model = xgb.train(params, dtrain, num_round)

# Делаем прогнозы
y_pred = model.predict(dtest)

# Оцениваем качество модели (например, среднеквадратичная ошибка)
mse = mean_squared_error(y_test, y_pred)
print(f'Среднеквадратичная ошибка: {mse:.2f}')

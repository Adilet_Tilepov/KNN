from sklearn.neighbors import KNeighborsClassifier

# Создаем модель
model = KNeighborsClassifier(n_neighbors=3)

# Обучаем модель на тренировочных данных
X_train = [[2000, 3], [1500, 2], [2500, 4], [1200, 2]]
y_train = ['дорогой', 'дешевый', 'дорогой', 'дешевый']
model.fit(X_train, y_train)

# Классифицируем новый дом
X_new = [[1800, 3]]
predicted_class = model.predict(X_new)
print("Класс нового дома:", predicted_class)
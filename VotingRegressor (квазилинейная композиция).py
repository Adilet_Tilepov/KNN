import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import VotingRegressor
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler

# Загрузка данных
data = {
    'Возраст': [18, 19, 20, 21, 18, 19, 20, 21, 18, 19],
    'Часы_учебы': [5, 4, 6, 3, 5, 6, 4, 3, 5, 6],
    'Оценка': [85, 88, 92, 78, 90, 87, 89, 76, 85, 88],
    'Количество_заданий_выполнено': [15, 20, 18, 22, 19, 17, 21, 20, 16, 23]
}

df = pd.DataFrame(data)

# Дополнение датасета вручную
additional_data = {
    'Возраст': [22, 23, 24, 20, 21, 19, 20, 22, 19, 21],
    'Часы_учебы': [4, 6, 5, 3, 4, 6, 3, 5, 4, 6],
    'Оценка': [80, 85, 88, 75, 82, 78, 85, 89, 76, 82],
    'Количество_заданий_выполнено': [12, 18, 15, 10, 14, 16, 11, 17, 13, 19]
}

# Добавление новых данных к существующему датафрейму
df = pd.concat([df, pd.DataFrame(additional_data)], ignore_index=True)

# Вывод обновленного датафрейма
print(df)

# Разделение данных на обучающую и тестовую выборки
X = df.drop('Оценка', axis=1)
y = df['Оценка']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Стандартизация признаков
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Создание базовых моделей
linear_model = LinearRegression()
tree_model = DecisionTreeRegressor(random_state=42)

# Создание квазилинейной композиции
ensemble_model = VotingRegressor(estimators=[
    ('linear', linear_model),
    ('tree', tree_model)
])

# Обучение модели
ensemble_model.fit(X_train_scaled, y_train)

# Предсказание на тестовой выборке
y_pred = ensemble_model.predict(X_test_scaled)

# Оценка качества модели
mse = mean_squared_error(y_test, y_pred)
print(f'Средне-квадратичная ошибка: {mse}')
